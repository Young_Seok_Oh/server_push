package com.example;

public interface RedisMessageReceiver {
	void receiveMessage(String message);
}
