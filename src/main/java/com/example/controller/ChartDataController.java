package com.example.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.RedisMessageReceiver;
import com.example.domain.ChartData;

@Controller
public class ChartDataController implements RedisMessageReceiver {

    @Autowired
    SimpMessagingTemplate msgTemplate;
    
    @RequestMapping("/chart")
    public String getChartDataPage() {
        return "chart";
    }
        
    public void receiveMessage(String message) {
        msgTemplate.convertAndSend("/graph/datasets", 
            new ChartData(Integer.parseInt(message)));
    }
}
