package com.example.domain;

public class ChartData {
    Integer data;

    public ChartData(Integer data) {
        this.data = data;
    }
    
    public Integer getData() {
        return data;
    }

}
