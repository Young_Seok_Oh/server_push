package com.example;

import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class RandomIntegerGenerator {

    private static final String CHANNEL = "DATASETS";
    
    @Autowired
    private StringRedisTemplate redisTemplate;
    
    private Random randomGenerator = new Random();
    
    @Scheduled(fixedRate = 1000)
    public void generate() {
        redisTemplate.convertAndSend(CHANNEL, 
            String.valueOf(randomGenerator.nextInt(100)));
    }
}
